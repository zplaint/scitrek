\section{Preliminaries}
\label{sec:preliminaries}

By $\zNbb$ we denote the sets of non-negative natural numbers.
For a set $S$, a \emph{bag (multiset)} $m$ over $S$ is a mapping$m:S\rightarrow \Nat$.
The set of all bags over $S$ is denoted by $\Nat^S$.
We denote addition and subtraction of two bags by $+$ and $-$,
the number of all elements in $m$ taking into account the multiplicity
by $\| m \|$, and comparisons of bags by $=, <, >, \leq, \geq$.
that are defined as
$m_1 \mathrel{R} m_2 \equiv \forall s \in S : m_1(s) \mathrel{R} m_2(s)$
where $\mathrel{R}$ is one of $=,<,>,\leq,\geq$.
We overload the set notation writing $\emptyset$ for the empty bag and $\in$
for the element inclusion.

Petri nets is a well-known formalism for concurrent systems modelling.
In this section, we give the definition of coloured Petri nets
with labelled transitions \cite{bookJensen2009} (CP-nets) parameterized
with a value universe $U$.
A coloured function for places is defined using the
notion of types, and a coloured function for arcs is
defined using expressions over the simple additive language $Expr$.
%In a coloured net
Each place is mapped to a type, which is a subset of $U$.
We assume a language $\Expr$ for arcs expressions over a set $\Var$ of variables
and a set $Con$ of constants with some fixed interpretation $\In$,
such that for any type-consistent evaluation $\nu : \Var \to U$
the value $\In(e,\nu) \in \Nat^U$ of an expression $e \in \Expr$ is defined.
We also assume a set $\Lab$ of
labels for transitions such that $\tau \not\in \Lab$.
The label $\tau$ is the special `silent' label,
while labels from $\Lab$ mark externally observable firings.
The $\tau$ labels are usually omitted on transitions.

\begin{definition}[Coloured Petri net]
A {\em coloured net} over the universe
$U$ is a 6-tuple $\langle P,T,F,\upsilon,\gamma,\Lambda \rangle$, where
\begin{itemize}
    \item $P$ and $T$ are disjoint finite sets of {\em places}, respectively,
        {\em transitions};
    \item $F \subseteq (P \times T) \cup (T \times P)$ is a set of {\em arcs};
    \item $\upsilon : P \to 2^U$ is a {\em place typing} function mapping  $P$
        to the subsets of $U$;
    \item $\gamma : F \to \Expr$ is an {\em arc labelling} function;
    \item $\Lambda : T \to \Lab\cup \{\tau\}$ is a {\em transition labelling} function.
\end{itemize}
\label{def:col:net}
\end{definition}

For an element $x \in P \cup T$, an arc $\langle y,x \rangle$ is called an
\textit{input arc}, and an arc $\langle x,y \rangle$ --- an \textit{output arc}.
A \textit{preset} $\zpreset{x}$ and a \textit{postset} $\zpostset{x}$ are
subsets of
$P \cup T$ such that $\zpreset{x} = \lbrace y \vert \langle y,x \rangle \in F\rbrace$
and $\zpostset{x} = \lbrace y \vert \langle x,y \rangle \in F\rbrace$.
Given a CP-net $N = \left<P,T,F,\upsilon,\gamma,\Lambda\right>$
over the universe $U$,
a {\em marking} in $N$ is a function $m: P\to \Nat^{U}$ such that $m(p)$
has $\upsilon(p)$ as a support set.
By $\mathfrak{M}_N$, we denote all valid markings of $N$.
A pair $\langle N, m \rangle$ of a CP-net and a marking is called a marked net.

Let $N = \langle P,T,F,\upsilon,\gamma,\Lambda \rangle$ be a CP-net.
A transition $t\in T$ is \textit{enabled} in a marking $m$
iff $\exists \nu \forall p\in P: \left< p,t \right> \in F \Rightarrow m(p)\geq \In(\gamma \left<p,t\right>,\nu)$.
Here $\nu : \Var \to U$ is a variable evaluation called a \textit{binding}.
An enabled transition $t$ can \textit{fire} yielding  a new marking
$m'(p)= m(p)-\In(\gamma\left<p,t\right>,\nu)+\In(\gamma\left< t,p \right>,\nu)$ for each $p\in P$
(denoted $m\ztoabove{t}m'$).
The set of all markings reachable from a marking $m$ (via a sequence of firings)
is denoted by $\mathfrak{R}(m)$.

%
As usual, a marked net defines a labelled transition system (LTS) which
represents the observable behaviour of the net; the transition system states
correspond to net markings, its transitions correspond to firings,
and such transitions are labelled with corresponding synchronization labels.
Also, note that PT-nets are a particular case of CP-nets with a singleton
universe.

Let $\left< N, m_0 \right>$ be a marked net with transitions labelled
by labels from $\Lab \cup \{\tau\}$.
By abuse of notation we write $m\ztoabove{\lambda}m'$, if firing of a
transition $t$, labelled with $\lambda \in \Lab\cup\{\tau\}$, transfers a
marking $m$ to a marking $m'$.
For $\lambda \ne \tau$, we write  $m \overset{\lambda}\Rightarrow{m'}$
if there is a sequence of firings
$m = m_1 \ztoabove{\tau} \dots \ztoabove{\tau} m_k \ztoabove{\lambda} m^1\ztoabove{\tau}
\dots \ztoabove{\tau} m^n = m'$, where $k,n \geq 1$.
We write $m \overset{\tau}\Rightarrow{m'}$ if there is a sequence of
firings $m = m_1 \ztoabove{\tau} \dots \ztoabove{\tau} m_k  = m'$,
where $k \geq 1$.
\begin{definition}
Let $\left< N_1, m_0^1 \right>$ and $\left< N_2, m_0^2 \right>$ be two marked
Petri nets with transitions labelled with alphabet $\Lab \cup \{\tau\}$.
Net $N_1$ $m$-simulates $N_2$ (denoted $N_1\sqsubseteq_m N_2$) iff
there is a relation $R \subseteq {\mathcal R}(N_1) \times \mathcal{R}(N_2)$
such that
\begin{enumerate}[1.]
	\item $\langle m_0^1, m_0^2 \rangle \in R$;
	\item for any $\langle m_1, m_2 \rangle \in R$,
        if $m_1\ztoabove{\lambda}m_1'$ in $N_1$, then
		\begin{enumerate}
			\item there exists $m_2'$ such that $m_2 \overset{\lambda}\Rightarrow{m_2'}$
				in $N_2$ and $\langle m_1', m_2' \rangle \in R$
			\item for each $m_2'$ such that $m_2 \overset{\lambda}\Rightarrow{m_2'}$
				in $N_2$ it should be $\langle m_1', m_2' \rangle \in R$
		\end{enumerate}
	%\item for any $(m_1, m_2) \in R$, if $m_1 \ztoabove{\tau} m_1'$ in $N_1$, then
	%\begin{enumerate}
		%\item $(m_1', m_2) \in R$
		%\item $(m_1, m_2') \in R$
	%\end{enumerate}
\end{enumerate}
\label{def_simm}
\end{definition}
\begin{definition}
Let $\left< N_1, m_0^1\right>$ and $\left< N_2, m_0^2 \right>$ be two marked Petri
nets with transitions labelled with alphabet
$\Lab \cup \{\tau\}$.
Nets $N_1$ and $N_2$ are $m$-bisimilar (denoted $N_1 \bisimm N_2$) iff
there is a relation $R \subseteq {\mathcal R}(N_1) \times \mathcal{R}(N_2)$
such that
\begin{enumerate}[1.]
	\item $\langle m_0^1, m_0^2 \rangle \in R$;\
    \item $N_1$ $m$-simulates $N_2$ with $R$, and, simultaneously,
        $N_2$ $m$-simulates $N_1$ with $R^{-1}$.
	%%\item for any $\langle m_1, m_2 \rangle \in R$, if $m_1\ztoabove{\lambda}m_1'$ in $N_1$, then
	%%\begin{enumerate}
		%%\item  there exists $m_2'$ such that
            %%$m_2 \overset{\lambda}\Rightarrow{m_2'}$ in $N_2$ and
            %%$\langle m_1', m_2' \rangle \in R$,
		%%\item for each $m_2'$ such that $m_2 \overset{\lambda}\Rightarrow{m_2'}$
            %%in $N_2$ it should be $\langle m_1', m_2' \rangle \in R$;
	%%\end{enumerate}
       %%
	%%\item and symmetrically, for any $\langle m_1, m_2 \rangle \in R$, if
        %%$m_2\ztoabove{\lambda} m_2'$ in $N_2$, then
		%%\begin{enumerate}
		%%\item  there exists $m_1'$ such that
            %%$m_1 \overset{\lambda}\Rightarrow{m_1'}$ in $N_1$ and
            %%$\langle m_1', m_2' \rangle \in R$,
		%%\item for each $m_1'$ such that
            %%$m_1 \overset{\lambda}\Rightarrow{m_1'}$ in $N_1$ it should be
            %%$\langle m_1', m_2' \rangle \in R$.
	%%\end{enumerate}
\end{enumerate}
\label{def_bisimm}
\end{definition}

Note that the m-bisimilarity relation is not reflexive.
The intrinsic reason for this is that the composition of components even with
the same behaviour may be not live.
%%%Nonetheless, it does not mean that less restrictive equivalences are of
%%%no use; au contre, they may reveal that the system is live, when
%%%the $m$-bisimulation cannot be established.

%%Justification for non reflexivity:
%%Imagine that in a P2P network, we have two peers that after contacting
%%each other may exchange data.
%%%Each agent, after sending an acknowledgement to its party, may start
%%%either upload or download.
%%%So the `a' action in Figure X would correspond to `acknowledgement',
%%%`b' --- to upload, and `c' --- to download activities.
%%%If the agents do not use some additional actions to arrange the direction
%%%of a transfer, then, after acknowledgement, both may enter `upload' state
%%%and this would lead to a confusion.
%%%%Nonetheless, it does not mean that less restrictive equivalences are of
%%%%no used; au contre, they may reveal that the system is live, when
%%%%the $m$-bisimulation cannot be established.
%%%%However, it could require more involved computations to check
%%%%the equivalence;
%%%%FIX
 %%%%%%i.e. it could shift from linear to polynomial
%%%%%%complexity, because it could not infer the property locally, only
%%%%%%from outgoing actions of a $\tau$-component, but it should analyse
%%%%%%cycles etc
%%%%%%%and the algorithm works over reachability graph, which can be exponentially
%%%%%%larger than an original net.
%%%%%
%%There are still less restrictive, that would distinguish such P2P-2-agents
%%problem, but would allow , for example,
%%P1: a.b.0+a.c.0  P2: a.(b.0+c.0), so the system
%%can do b or c finally in any stage.
%%However, do we need to allow such cases or not is problem-specific.
%%If P2 is a choosing side then we have a problem in the P1||P2 system.
%
%%So we stop on m-bisimulation.
%%All further playing with equivalence preorders is postponed for further research
%%Depends on how much freedom or controlability we want to give to the partners
%%of composition.
%%For more details see (glabbeek, on good preorders/congruence, lin spectrum)
