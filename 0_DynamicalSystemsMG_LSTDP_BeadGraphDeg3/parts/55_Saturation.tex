\section{Saturation time for bead $\mathit{DP}$-systems with incommensurable
edge lengths}

$\mathit{DP}$-system on a metric graph with incommensurable edges
(\textit{i.e.}, the rank of its edges over $\zQbb$ is greater than one)
never stabilizes and, moreover, ergodic
\cite{ChernyshevTolchennikovShafarevich16_BehaviorQuasiParticlesHybridSp_RelGeomGeodesicsAnalyticNT}.
However, it is possible to extend the notion of stabilization time to
systems with incommensurable edges using the notion of $\varepsilon$-net
\cite{hausdorff_set_2005}.

\begin{figure}[!t]
    \begin{center}
        \centerline{\includegraphics[scale=0.9]{img/mg_epsilonnet.pdf}}
        %\centerline{\includegraphics[scale=0.9]{Fig10.pdf}}
        \caption{Edge $\langle v_1,v_2 \rangle$ with
            $\varepsilon$-net $N_{\varepsilon}$ depicted as gray points}
        \label{fig:mg_epsnet}
    \end{center}
\end{figure}


For a point $x$ on $\Gamma$, a closed 1-ball $B_{\varepsilon}(x)$ with
radius $\varepsilon$ around $x$ is the set of all points in $\Gamma$
whose distance from $x$ in the metric $\rho$ is less than $\varepsilon$,
\textit{i.e.,} $\{p\in\Gamma | \rho(p,x)\le \varepsilon \}$.
A finite subset $N_{\varepsilon}$ of points of metric graph $\Gamma$ is
an \emph{$\varepsilon$-net} if, for each point $p\in \Gamma$, distance
$\rho(p,N_{\varepsilon})$ is less than $\varepsilon$.
Thus, the closed 1-balls with centres in $N_{\varepsilon}$ cover
the whole $\Gamma$.
Now we consider \emph{$\varepsilon$-net} over point-places,
and 1-ball $B_{\varepsilon}(x)$ are set of points at $\varepsilon$-distance or
less from moving point-place $x$.
In Figure~\ref{fig:mg_epsnet}, edge $\langle v_1, v_2\rangle$ holds only one
dynamic point $p$, which saturates 1-ball (\textit{i.e.,} line segment bounded
by the circle) $B_{\varepsilon}(x_1)$;
note that, points of $\varepsilon$-net, coloured gray,
are not necessarily regularly distributed.
For $\mathit{DP}$-system $\mathcal{P}_\Gamma$ and $\varepsilon$-net
$N_{\varepsilon}$ on point-places of $\Gamma$,
the saturation time $t_{s}(N_{\varepsilon})$ of $\mathcal{P}_\Gamma$ is
the earliest point in time when, for each point $x$ in $N_{\varepsilon}$,
there is a point $q$ in $\mathcal{P}_\Gamma$ with $\rho(x,q) \le \varepsilon$.
For given $\varepsilon$, the saturation time $t_{s}(\varepsilon)$ of
$\mathcal{P}_\Gamma$ is the supremum of the set of saturation times for all
possible $\varepsilon$-nets on $\Gamma$,
\textit{i.e.}, $t_{s}(\varepsilon) = \sup\{t_{s}(N_{\varepsilon})\}$.
We call 1-ball $B_{\varepsilon}(x)$ on $\Gamma$ with centre $x$ and radius
$\varepsilon$ \emph{saturated} when point $p$ of $\mathcal{P}_\Gamma$
is bound to a place-point in $B_{\varepsilon}(x)$.

\begin{theorem}
Let $E$ be a given set of edges with a length function $l$ on $E$,
and the rank of $E$ over $\zQbb$ is greater than one.
There is a $\mathit{DP}$-system on a bead metric graph with vertices of degree
three or less and with an initial point in one of its terminal vertices
that has the longest saturation time among systems in $\mathit{DP}(E)$.
\label{coro:fin_satur}
\end{theorem}

\begin{proof}
At time $t_{s}(\varepsilon)$, points of $\mathcal{P}_\Gamma$ form an
$\varepsilon$-net themselves; if they do not, it is possible to construct
$N_{\varepsilon}$ that is not saturated at $t_{s}(\varepsilon)$ by taking the centre of a non-covered
ball into $N_{\varepsilon}$, which contradicts the definition of
$t_{s}(\varepsilon)$.
Thus, $t_{s}(\varepsilon)$ is the time point when all the non-saturated balls
vanish.
Fix ball $B_{\varepsilon}(x)$ that is saturated at $t_{s}(\varepsilon)$
by dynamic point $q$ and
build $\varepsilon$-net $N_{\varepsilon}$ such that,
within $B_{\varepsilon}(x)$,
only the centre of $B_{\varepsilon}(x)$ belongs to $N_{\varepsilon}$.
Let $q_0$ in vertex $v_0$ be the ancestor of $q$ in the initial state of
$\mathcal{P}_\Gamma$,
$e_s$ be an edge containing centre $x$, and $p$ be the path of $q$
from $v_0$ to $B_{\varepsilon}(x)$.
By the graph surgery suggested in
Lemma~\ref{thm:DP_bead}--Theorem~\ref{coro:fin},
we build a bead graph with vertex degrees not greater than three that preserves
$v_0e_s$-path $p$.
\end{proof}

