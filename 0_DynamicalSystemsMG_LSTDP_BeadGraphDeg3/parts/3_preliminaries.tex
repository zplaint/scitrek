\section{Preliminaries}
\label{sec:preliminaries}

%By $\zNbb$ we denote the set of non-negative natural numbers.
%For a set $S$, a \emph{bag (multiset)} $m$ over $S$ is a mapping
%$m:S\rightarrow \Nat$.
%The set of all bags over $S$ is denoted by $\Nat^S$.
%We denote addition and subtraction of two bags by $+$ and $-$,
%the number of all elements in $m$ taking into account the multiplicity
%by $\| m \|$, and comparisons of bags by $=, <, >, \leq, \geq$.
%that are defined as
%$m_1 \mathrel{R} m_2 \equiv \forall s \in S : m_1(s) \mathrel{R} m_2(s)$
%where $\mathrel{R}$ is one of $=,<,>,\leq,\geq$.
%We overload the set notation writing $\emptyset$ for the empty bag and $\in$
%for the element inclusion.

A metric graph $\Gamma$ is a graph consisting of set of vertices $V$,
set of undirected edges $E$,
and length function $l$ mapping each edge $e=\{ v_{1},v_{2}\} \in E$
to a positive real,
i.e., $l: E \to \mathbb{R_+}$ \cite{BerkolaikoKuchment13_IntroQuantum}.
For technical convenience, %when needed,
each undirected edge $e=\{ v_{1},v_{2}\}$ in $E$ may be considered as
a pair of arcs $\langle v_{1},v_{2}\rangle$ and $\langle v_{2},v_{1}\rangle$,
and both arc lengths coincide with the length of $e$;
both notions will be used in the paper interchangeably to
shorten some lengthy technical explanations.

The arc opposite to arc $a=\langle v_j,v_i \rangle$ is denoted by $\bar{a}$,
i.e., $\bar{a}=\langle v_i,v_j \rangle$.
For two points $x$ and $y$ on the graph, metric $\rho(x,y)$ is the shortest distance
between them, where distance is measured along the edges of the graph
additively.
A walk is a finite or infinite sequence of edges (arcs for directed graphs)
which joins a sequence of vertices.
A trail (path) is a walk in which all edges (vertices) are distinct.

The set of all walks from vertex $v$ to vertex $v'$ is denoted by
$\mathcal{W}(v,v')$.
For a walk $w$, the length $l(w)$ is the sum of lengths of all the arc entries in $w$.
The support of walk $w$ is the set of all arcs in $w$ and is denoted by $S(w)$.
The (directed) multisupport of walk $w$ in graph $\Gamma$ is a new (directed)
graph $\overline{S}(w)$ obtained from $\Gamma$ such that it has the same vertices $V$ as
$\Gamma$, and, for each entry of arc $\langle v_i, v_j \rangle$ in $w$,
we introduce a new edge $\{ v_i, v_j \}$
(new arc $\langle v_i, v_j \rangle$) into $\overline{S}(w)$,
i.e., the number of edges (arcs) in $\overline{S}(w)$ is equal to the
number of arc entries in $w$.
Note that a walk may contain multiple entries of an arc.

%an interval $[0,L_e]$, and $x_{e}$ is the coordinate on the interval
%. 
%%%Vertex $v_{1}$ corresponds to $x_e=0$, and $v_{2}$ to $x_e=L_e$,
%%%or vice versa.
%%%The choice of which vertex lies at zero is arbitrary with the alternative
%%%corresponding to a change of coordinate on the edge.

\begin{figure}[!t]
    \begin{center}
        \centerline{\includegraphics[scale=1]{img/mg.pdf}}
        %\centerline{\includegraphics[scale=1]{Fig1.pdf}}
        \caption{System of dynamic points $\zPcal_\Gamma$
          on metric graph $\Gamma$}
        \label{fig:mg}
    \end{center}
\end{figure}

For a metric graph $\Gamma$, the dynamics of a system of dynamic points
$\mathcal{P}_\Gamma$ on $\Gamma$
is defined as following.
In the initial state, some vertices of $\Gamma$ hold a dynamic point.
When time starts to flow, each such point $p$ located in vertex $v$,
for each edge $e$ incident to $v$,
produces a point $p'$ on each $e$, and $p$ disappears
(intuitively, this corresponds to wave packet scattering);
each produced point $p'$ starts moving along corresponding~$e$.
Note that if we consider $e$ as a pair of directed arcs,
then $p'$ is generated on and moving along the arc outgoing from $v$
respecting the arc direction.
All points move with the same constant velocity; and,
due to new points generation, some arcs may carry more than one point.
When a moving point reaches vertex $v'$, again,
on each outgoing arc incident to $v'$, a new point is generated.
When more than one points reach a vertex simultaneously at $t$,
on each outgoing arc, only one point is produced,
as if only one point has reached the vertex at $t$;
i.e., points met on a vertex fuse, and each coordinate of an arc can carry
only one dynamic point.
However, points do not collide anywhere on edges except vertices,
i.e., if two points met on an edge,
they both continue their movement towards their own directions.
This becomes clearer if we consider the edge as a pair of arcs;
then, the points, converging on an edge, are moving along separate opposite arcs.
In Figure~\ref{fig:mg}, the initial set of points consists of two points in
vertices $v_1$ and $v_3$.
The point in $v_1$ produces a new point on edge $\{ v_1,v_2 \}$,
The point in $v_3$ produces points on edges leading to vertices
$v_2,v_4,v_5,v_6,v_7$.
After a time unit, there are no points in $v_1$ and $v_3$ (coloured gray),
but there are points (coloured black) moving from $v_1$ and $v_3$ 
to their adjacent vertices.

More examples and further details on $\mathit{DP}$-systems on metric graphs
and some of their extensions can be found in
\cite{Chernyshev17_SecondTermNumberPointsMetricGraph,
Chernyshev17_CorrectionCountingMetricTree,
ChernyshevTolchennikovShafarevich16_BehaviorQuasiParticlesHybridSp_RelGeomGeodesicsAnalyticNT}.
