del /S *.aux *.bbl *.bcf *.glo *.ist *.out *.toc *.synctex* *.spl
del /S *.blg *.log *.xml
del /S *~
del /S *.tikz.preview.pdf
